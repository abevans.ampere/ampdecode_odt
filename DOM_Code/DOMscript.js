/*
JS DOM
The Document Object Model, abbreviated DOM, is a powerful tree-like structure that organizes the elements 
on a web page and allows scripting languages to access them.

    The DOM is a logical tree-like Model that organizes a web page’s HTML Document as an Object.

    The DOM is implemented by browsers to allow JavaScript to access, modify, and update the structure 
    of an HTML web page in an organized way.


a node is the equivalent of each family member in a family tree. 
a node is an intersecting point in a tree that also contains data.

ATTRIBUTES of Element Nodes

the DOM allows us to access a node’s attributes, such as its class, id, and inline style.

OPERATIONS ON ELEMENT NODES
if we were accessing that element node in our script, the DOM would allow us to tweak each of those attributes.

DOCUMENT KEYWORD

The document object allows you to access the root node of the DOM tree. 

Before you can access a specific element in the page, first you must [access the document structure itself].

The document object allows scripts to access children of the DOM as properties.

document object - allows scripts to access and modify nodes/elememts in a webpage.

*/


/*
    ACCESSING & MODIFYING THE DOM

    access & modified the document object's (model) <body> element

    creating <h1></h1>, <h2></h2> & <p></p> elements/nodes within DOM, <body> element/node.
*/


document.body.innerHTML = `<h1></h1> 
                            <h2></h2>
                            <p></p>`

/**
* SELECT & MODIFY ELEMENTS

    DOM interface allows us to access specific elements with CSS selectors.

    - CSS selectors define the elements to which a set of CSS rules apply
        access CSS selectors in JS -> [tag names, .classes, #id(s)]

        .querySelector('element') - method allows for CSS selector specification
            as string and returns the first element that matches the selector.

        i.e. - .querySelector('p')   

        can store each accessed DOM element within a variable declaration.
 */


// selecting the first html h1 element and assigning it text.
document.querySelector('h1').innerHTML = 'DOM Heading Node 1'

// selecting the first html h2 element and assigning it text.
document.querySelector('h2').innerHTML = 'DOM Heading Node 2'

// selecting the first html paragraph element and assigning it text.
document.querySelector('p').innerHTML = 'DOM paragraph node 3.'






/**
 * ACCESS & STYLE ELEMENTS
 * 
 * '.style' property of a DOM-object element provides access to the inline style of that HTML tag.
 * 
 * syntax - element.style.property (property represents a CSS property).
 * 
 * //setting style property.
 * document.body.style.styleProp = 'style'
 * 
 * 
 * //accessing a DOM element's class or id and applying css style property.
 * document.querySelector('.class/#id').style.styleProp = ''
 */

document.body.style.backgroundColor = 'silver'

document.querySelector('h1').style.color = 'green'

document.querySelector('h2').style.color = 'white'

document.querySelector('p').style.color = 'green'




/**
 * TRAVERSE THE DOM
 * 
 * '.parentNode' property returns the parent of the specified element in the DOM hierarchy.
 * 
 * '.children' property returns an array of the spec parent element/node's children(nested elements).
 * 
 * 
 * 
 * //accessing element with id list-element's parentNode.
 * 
 * let parentElement = document.getElementById('list-element').parentNode
 * 
 * // accessing element with id 'unordered's children [nodes].
 * 
 * let childElement = document.getElementById('unordered').children
 * 
 */


/**
 *  CREATE AND INSERT ELEMENTS
 * .createElement() method creates a new element based on the specified tag name passed
 * into it as an argument.
 * 
 * newElement.id = 'idName' - id property assigns an id name to an element.
 * must assign the newly created element to an existing DOM child element (new parent element).
 * 
 * 
 * creating, setting id, creating content, appending, and styling a new html/DOM element.
 */

let newPpg = document.createElement('p')
newPpg.id = 'new-ppg'
newPpg.innerHTML = 'DOM child paragraph'
document.body.appendChild(newPpg)
document.getElementById('new-ppg').style.color = 'white'


/**
 * REMOVING AN ELEMENT
 * .removeChild() method removes a specified child from a parent.
 * 
 * removing an element from the <body> element.
 */
document.body.removeChild(newPpg)

/**
 * ADD CLICK INTERACTIVITY
 * 
 * create, style and apply functionality/event to a new button element.
 * 
 */

let modBackgroundColor = document.createElement('button')

modBackgroundColor.id = 'modify-bgColor'

modBackgroundColor.innerHTML = 'Modify Background Color'

document.body.appendChild(modBackgroundColor)

// Object  
// document.getElementById('modify-bgColor').onclick = ()=>{
//     document.body.style.backgroundColor = 'white'
// }
function modBgcBlack(){
    document.body.style.backgroundColor = 'black'
}
modBackgroundColor.addEventListener('click', modBgcBlack)


function modBgcWhite(){
    document.body.style.backgroundColor = 'white'
}
modBackgroundColor.addEventListener('dblclick', modBgcWhite)


/**
 * EVENTS
 * 
 * EVENTS - user interactions and browser manipulation
 * that can be programmed to fire or trigger functionality.
 * 
 * i.e. - mouse clicking on a button
 *      - webpage files loading in the bowser
 *      - a user swiping right on an image
 * 
 * i.e. - 'click event' fired when a user clicks a button.
 * 
 * 
 * 
 * 
 * FIRING EVENTS
 * 
 * 1. spec event fires within the DOM.
 * 
 * 2. spec event fires when the browser is reloaded.
 * 
 * 3. event handler fn() can be created to run as a response.
 * event handler functions modify and update DOM elements after an event fires.
 * 
 * 
 * 
 * EVENT HANDLER REGISTRATION
 * 
 * 1. event-handlers create interactivity.
 * 
 * 2. addEventListener() - can have a DOM element listen for a spec event
 * and execute a block of code when event is detected.
 * 
 * 3. event target - DOM element that listens for an event.
 * 
 * 4. event-handler - block of code that runs when the event happens.
 * 
 * 
 * 
 * 
 * ADDING EVENT HANDLERS
 * 
 * 1. event handlers can be registered by settings an .onevent property on a DOM element.
 * 
 * method-a. element.onevent = eventhandlerFunction
 * method-b. element.addEventListener('event', eventHandlerFunction)
 * 
 * 
 * 
 * 
 * REMOVING EVENT HANDLERS
 * 
 * .removeEventlistener() - removes and/or disables the .addEventListener() method.
 * stops the event target from 'listening' for an event to fire when no longer needed.
 * 
 * 
 * let displayElement = document.createElement('h1')
 * 
 * displayElement.id = 'display-output'
 * 
 * let actionElement = document.createElement('button')
 * 
 * actionElement.id = 'action'
 * 
 * document.body.appendChild(displayElement)
 * 
 * document.body.appendChild(actionElement)
 * 
 * function modifyDOM(){
 *     displayElement.innerHTML = 'output data'
 *     document.re
 * }
 * actionElement.addEventListener('click', modifyDOM)
 * 
 * 
 * 
 * 
 * 
 */

 /**
 * EVENT OBJECT PROPERTIES
 * 
 * js stores Event-objects 
 *  - related data
 *  - functionality
 *  - methods
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */












