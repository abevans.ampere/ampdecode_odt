/*
    *goal:
        convert 8 (s0/s1) termination values to hex-values.
        check if input_integer/drop-down converts to valid hex-code.
    
        from a 32-bit number. extract a larger number in hexadecimal number.

    *target functionality:

        to test user-input, write a function that takes in an integer and runs a calculation, 
        using termination values. software then validates successful hex-code output.

        *development approaches:

            > object oriented programming.
                > setter method that stores input-string that is converted into b10 integer.

                > every input-string-array element needs to be compared against object.property.key:value pairs.

                > convert every contained string element into a hexadecimal value.

                > find sum-like hex value calculation.

                > apply function to each form-option element.
                    > capture input, convert, compare, calculate, output to screen.

            > conversion test; string > ohm-object > integer > hex-code.

            > capture sum of all hex values to output final target hex-value.
                > utilize int gathering & hex conversion.


    *current application functionality status:

        > conversions: string > integer.
        > conversions: base_10 interger > base_16/hex number for each rtt_group termination.
        > software output: rtt terminations output hex-code in string format.
*/



/*prompt asks user for string-int input.
convert to html page dropdown option element.*/
let targetInput = prompt('enter an integer.')


/* simulates html element options.
 * apply html element/selection values to html buttons.
 * convert options to integers.
*/

const ohmsArr = [
    '0:0Ω',
    '1:240Ω',
    '2:120Ω',
    '3:80Ω',
    '4:60Ω',
    '5:48Ω',
    '6:40Ω',
    '7:34Ω'
]

let newOhmValsArr = []

// function that converts/matches string values to ohm values. 
function convertToOhms(inputParam, arrayParam){
    //future-dev: apply number-string values to DOM elements to match ohm-string values when selected.//
    for(let i = 0; i < inputParam.length; i++){
        //refactor
        inputParam[i] === '0' ? arrayParam.push(ohmsArr[0]) : ' '

        inputParam[i] === '1' ? arrayParam.push(ohmsArr[1]) : ' '

        inputParam[i] === '2' ? arrayParam.push(ohmsArr[2]) : ' '

        inputParam[i] === '3' ? arrayParam.push(ohmsArr[3]) : ' '

        inputParam[i] === '4' ? arrayParam.push(ohmsArr[4]) : ' '

        inputParam[i] === '5' ? arrayParam.push(ohmsArr[5]) : ' '

        inputParam[i] === '6' ? arrayParam.push(ohmsArr[6]) : ' '

        inputParam[7] === '0' ? arrayParam.push(ohmsArr[7]) : ' '
    }
    //logs out new array with extracted & appended values.
    console.log(arrayParam)
}
convertToOhms(targetInput, newOhmValsArr)

console.log('----')


//target termination variables.
let rtt_nom_rd_s0, rtt_nom_wr_s0, rtt_park_s0, rtt_wr_s0, Rtt_nom_rd_s1, Rtt_nom_wr_s1, Rtt_park_s1, Rtt_wr_s1


//** actual/target conversion values will be form/selection/drop down values. **//
//function that collects string input into an array > convert strings into integers > ouputs hexadecimal code.

function validateInput(target){
        //setting new target array that captures strings that are coverted integers.
        let intArr = []

        //iteration over an input string.
        for(let j = 0; j < target.length; j++){

            //adding converted string numbers to capturing empty array.
            intArr.push(target[j])
        }

        //log out
        console.log(
            intArr
        )

    /* 
        input values must match list of numbers provided within drop down values.
            combine output values into an array/object
            testValues = [0, 1, 2, 3, 4, 5, 6, 7]
            
            
            interate through and convert all elements into integers
            iterate and add all elements
            convert final sum into hex-value.

            terminations converted to hex from base10 numbers.

        termination variables conversion.
        
        converting and reassigning termination variables with bitwise operations.
        
        based on html element DIMM slot and rank, 
        may need to divide termination variables into two groups.
    */

    rtt_wr_s0 = 7 & target;

    rtt_nom_rd_s0 = (7 << 3 && target) >> 3;

    rtt_nom_wr_s0 = (7 << 6 && target) >> 6;

    rtt_park_s0 = (7 << 9 && target) >> 9;

    //

    Rtt_wr_s1 = (7 << 16 && target) >> 16;

    Rtt_nom_rd_s1 = (7 << 19 && target) >> 19;

    Rtt_nom_wr_s1 = (7 << 22 && target )>> 22;

    Rtt_park_s1 = (7 << 25 && target) >> 25;


// refactor operation
    let containTerms = []
    
    containTerms.push(
        rtt_nom_rd_s0, 
        rtt_nom_wr_s0, 
        rtt_park_s0, 
        rtt_wr_s0, 

        Rtt_nom_rd_s1,
        Rtt_nom_wr_s1, 
        Rtt_park_s1, 
        Rtt_wr_s1
    )
    
    for(let k = 0; k < containTerms.length; k++){
        let modifiedTerm = parseInt(containTerms[k], 10).toString(16)
        console.log(`${modifiedTerm}`)
    }

/*
    rtt_wr_s0 = parseInt(rtt_wr_s0, 10).toString(16)

    rtt_nom_rd_s0 = parseInt(rtt_nom_rd_s0, 10).toString(16)

    rtt_nom_wr_s0 = parseInt(rtt_nom_wr_s0, 10).toString(16)

    rtt_park_s0 = parseInt(rtt_park_s0, 10).toString(16)



    // 

    Rtt_wr_s1 = parseInt(Rtt_wr_s1, 10).toString(16)

    Rtt_nom_rd_s1 = parseInt(Rtt_nom_rd_s1, 10).toString(16)

    Rtt_nom_wr_s1 = parseInt(Rtt_nom_wr_s1, 10).toString(16)

    Rtt_park_s1 = parseInt(Rtt_park_s1, 10).toString(16)

    
    

    //log out

    console.log(

        `\nrtt_wr_s0 = ${rtt_wr_s0}

        \nrtt_nom_rd_s0 = ${rtt_nom_rd_s0}

        \nrtt_nom_wr_s0 = ${rtt_nom_wr_s0}

        \nrtt_park_s0 = ${rtt_park_s0}

        \nRtt_wr_s1 = ${Rtt_wr_s1}

        \nRtt_nom_rd_s1 = ${Rtt_nom_rd_s1}

        \nRtt_nom_wr_s1 = ${Rtt_nom_wr_s1}

        \nRtt_park_s1 = ${Rtt_park_s1}`
    );
*/

    containTerms = []

    //array is now populated with indexed elements.
    containTerms.push(
        rtt_nom_rd_s0, 
        rtt_nom_wr_s0, 
        rtt_park_s0, 
        rtt_wr_s0, 

        Rtt_nom_rd_s1,
        Rtt_nom_wr_s1, 
        Rtt_park_s1, 
        Rtt_wr_s1
    )
    console.log()
    
    
    //sum of all elements.
    //utilize map(()=>{}) to convert all containTerms array elements into base16 decimals/numbers.
    //termConvergence is now a cloned array in memory and does not modify the original containTerms array.
    let termConvergense = containTerms.map((term)=>{
      return term ? term = parseInt(term, 16) : term = false
    
    }).reduce((previous, current)=>{
        return previous + current
    }, 0)
    
    //converting final base10 value to hexadecimal/base16 string value.
    termConvergense = parseInt(termConvergense, 10).toString(16)


    //converted arrays sum & final output.
    // console.log(termConvergense)

/*
    //function that will output final hex-code value to the section-header-element.
    function logOut(){

        var hexCodeOutput = document.getElementById('hex-code-output-header')

    }
*/

    //event-target

    //implement oop; create new DOM element using factory function.
    let convertBtn = document.getElementById('convert')
    

    //method that outputs final conversion value.
    function outputConvert(){

        document.getElementById('hex-code-output-header').innerHTML = `${termConvergense}`
    }

    

    //event target takes in a fn() that invokes an innerHTML-prop text.
    convertBtn.onclick = outputConvert
    /*
        alternative event-handler process
        convertBtn.addEventListener('click', outputConvert)
    */

    //event-handler-function
    function clearOutput(){
        //event-handler - block of code that runs when the event is fired.
        document.getElementById('hex-code-output-header').innerHTML = ''
    }

    convertBtn.addEventListener('dblclick', clearOutput)


}

//function call
validateInput(targetInput)










